﻿using A4_BigSchools.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace A4_BigSchools.ViewModels
{
    public class CourseViewModel
    {
        [Required]
        public String Place {  get; set; }
        [Required]
        [FutureDate]
        public String Date { get; set; }
        [Required]
        [ValidTime]
        public String Time { get; set; }
        [Required]
        public byte Category { get; set; }
        public IEnumerable<Category> Categories{ get; set; }
        public DateTime GetDateTime() {
            return DateTime.Parse(string.Format("{0} {1}",Date,Time));
        }
    }
}